/* eslint-disable-next-line no-unused-vars */
import React from 'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ArtistSearchPage } from './pages/ArtistSearchPage'
import { ArtistProfilePage } from './pages/ArtistProfilePage'
import { NotFoundPage } from './pages/NotFoundPage'

const AppRouter = () => (
  <Router>
    <Switch>
      <Route path="/" exact component={ArtistSearchPage}/>
      <Route path="/:artistId" exact component={ArtistProfilePage}/>
      <Route component={NotFoundPage} />
    </Switch>
  </Router>
)

export default AppRouter
