/* eslint-disable-next-line no-unused-vars */
import React from 'react'
import { useMediaQuery } from 'react-responsive'
import { SectionList } from './../SectionList/SectionList';
import { ArtistSong } from './ArtistSong';
import { AppleMusicBtn } from '../ListenOnAppleMusicBtn/ListenOnAppleMusicBtn';
import { lineClamp } from '../../utils/LineClamp';

const greishBorder = '1px solid rgb(144, 144, 144, 0.6)'

export const ArtistAlbum = props => {
  const isMobile = useMediaQuery({
    query: '(max-device-width: 800px)'
  })
  const { album = {} } = props
  const styles = {
    component: {
      ...(props.styles || {})
    },
    wrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center'
    },
    left: {
      wrapper: {
        margin: '0 0.4em',
        maxWidth: '40vw'
      },
      cover: {
        borderRadius: '5px',
        width: '512px',
        maxWidth: '40vw'
      },
      songCount: {
        color: 'rgb(144, 144, 144)',
        textTransform: 'capitalize'
      },
      editorsNotes: {
        wrapper: {
          borderTop: greishBorder
        },
        title: {
          color: 'rgb(14, 14, 14)',
          textTransform: 'uppercase',
          fontWeight: 700,
          fontFamily: `'Open Sans', sans-serif`
        },
        content: {
          maxWidth: '512px',
          fontSize: '1em',
          color: 'rgb(14, 14, 14)',
          textAlign: 'justify',
          ...(isMobile ? lineClamp(3) : {})
        }
      }
    },
    right: {
      wrapper: {
        flexGrow: 1,
        margin: '0 0.4em',
        maxWidth: '50%'
      },
      title: {
        fontWeight: 600,
        margin: isMobile ? 0 : '0.5em 0 1em 0'
      },
      appleMusicBtn: {
        margin: '2em 0'
      }
    },
    mobile: {
      padding: '0 0.6em'
    }
  }
  const songCount = album.songs.length

  const albumList = (<SectionList>{album.songs.map((song, k) => <ArtistSong song={song} key={k}/>)}</SectionList>)

  const appleMusicBtn = (<div style={styles.right.appleMusicBtn}><AppleMusicBtn onClick={() => null}/></div>)

  const editorsNotes = (<div style={styles.left.editorsNotes.wrapper}>
    <p style={styles.left.editorsNotes.title}>
      Editor's notes
    </p>
    <p style={styles.left.editorsNotes.content}>
      {album.editorsNotes}
    </p>
  </div>)

  const desktop = (<div style={styles.wrapper}>
    <div style={styles.left.wrapper}>
      <img style={styles.left.cover} src={album.coverUrl} alt="album cover"/>
      {!isMobile && <p style={styles.left.songCount}>
        {songCount} song{songCount === 1 ? '' : 's'}
      </p>}
      {!isMobile && editorsNotes}
    </div>
    <div style={styles.right.wrapper}>
      <h2 style={styles.right.title}>{album.title}</h2>
      {!isMobile && appleMusicBtn}
      {!isMobile && albumList}
    </div>
  </div>)
  return <div style={styles.component}>
    {desktop}
    {isMobile &&
      <div style={styles.mobile}>
        {appleMusicBtn}
        {editorsNotes}
        {albumList}
      </div>
    }
  </div>
}
