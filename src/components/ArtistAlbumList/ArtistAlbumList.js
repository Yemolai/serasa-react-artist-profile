/* eslint-disable-next-line no-unused-vars */
import React from 'react'
import { SectionList } from '../SectionList/SectionList'
import { ArtistAlbum } from './ArtistAlbum'

export const ArtistAlbumList = props => {
  const { albums = [] } = props.artist
  const styles = {
    list: {},
    album: {
      marginBottom: '2em'
    }
  }
  return <SectionList style={styles.list}>
    {albums.map((album, k) => <ArtistAlbum styles={styles.album} album={album} key={k}/>)}
  </SectionList>
}
