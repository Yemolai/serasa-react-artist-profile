/* eslint-disable-next-line no-unused-vars */
import React from 'react'
import moment from 'moment'

const doubleDigits = num => Number(num).toFixed(0).padStart(2, 0)

const toTime = seconds => {
  const duration = moment.duration(seconds * 1000)
  const hours = Math.floor(duration.asHours())
  const hoursStr = (hours > 0 ? doubleDigits(hours) + ':' : '')
  const minutesStr = doubleDigits(Math.floor(duration.asMinutes()) % 60)
  const secondsStr = doubleDigits(duration.asSeconds() % 60)

  return `${hoursStr}${minutesStr}:${secondsStr}`
}

const greyish = 'rgba(144, 144, 144, 0.5)'
const greishBorder = '1px solid ' + greyish

export const ArtistSong = props => {
  const styles = {
    li: {
      display: 'flex',
      displayDirection: 'row',
      displayGrow: 1,
      width: '100%',
      marginBottom: '-2px'
    },
    cover: {
      height: '64px',
      width: 'auto',
      borderRadius: '6px',
      margin: '5px'
    },
    wrapper: {
      borderTop: greishBorder,
      ...(props.last ? {borderBottom: greishBorder } : {}),
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '100%',
      marginLeft: '5px',
      paddingTop: '5px',
    },
    details: {
      display: 'flex',
      flexDirection: 'column',
      alignContent: 'center',
      flexGrow: 1
    },
    artists: {
      margin: '0 0 0.5em 0',
      color: greyish
    },
    title: {
      margin: '0.5em 0 0 0'
    },
    duration: {
      wrapper: {
        margin: '0 0 0 auto',
        height: '100%',
        flexShrink: 0
      },
      p: {
        color: greyish
      }
    }
  }
  const { duration = 0 } = props.song
  const songDuration = typeof duration === 'number' ? toTime(duration) : duration
  return <li className="album-song" style={styles.li}>
    <img
      className="album-song__album-cover"
      src={props.song.coverUrl}
      style={styles.cover}
      alt="album cover"
    />
    <div className="album-song__wrapper" style={styles.wrapper}>
      <div className="album-song__song-details" style={styles.details}>
        <p className="album-song__song-details__title" style={styles.title}>
          {props.song.title}
        </p>
        <p className="album-song__song-details__artists" style={styles.artists}>
          {props.song.artists.join(', ')}
        </p>
      </div>
      <div className="album-song__song-duration" style={styles.duration.wrapper}>
        <p style={styles.duration.p}>
          {songDuration}
        </p>
      </div>
    </div>
  </li>
}
