/* eslint-disable-next-line no-unused-vars */
import React from 'react'
import { useMediaQuery } from 'react-responsive'
import { SectionList } from '../SectionList/SectionList'
import { SectionListItem } from '../SectionList/SectionListItem'
import { lineClamp } from '../../utils/LineClamp'
import { AppleMusicBtn } from '../ListenOnAppleMusicBtn/ListenOnAppleMusicBtn'

export const ArtistBio = props => {
  const { artist = {} } = props
  const isMobile = useMediaQuery({
    query: '(max-device-width: 800px)'
  })
  const styles = {
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      padding: isMobile ? '0 5px' : 0
    },
    columns: {
      display: 'flex',
      flexDirection: isMobile ? 'column' : 'row'
    },
    bio: {},
    name: {},
    about: {
      alignText: 'justify',
      ...(isMobile ? lineClamp(3) : {} )
    },
    sidepanel: {
      minWidth: '25%',
      padding: isMobile ? 0 : '0 2em'
    }
  }
  const appleBtn = <div className="artist__bio__onAppleMusicBtn"><AppleMusicBtn verb="View"/></div>
  return <div className="artist__bio" style={styles.wrapper}>
    <h1 className="artist__bio__about__name" aria-label="name" style={styles.name}>
      {artist.name}
    </h1>
    <div style={styles.columns}>
      <div className="artist__bio__about" aria-label="artist bio" style={styles.bio}>
        <p aria-label="artist__bio__about__content" style={styles.about}>
          {artist.about}
        </p>
      </div>
      {isMobile ? appleBtn : null}
      <div className="artist__bio__details" style={styles.sidepanel}>
        <SectionList>
          <SectionListItem label="Origin">
            {artist.origin}
          </SectionListItem>
          <SectionListItem label="Genre">
            {artist.genre}
          </SectionListItem>
          <SectionListItem label="Born">
            {artist.birthDate}
          </SectionListItem>
        </SectionList>
      </div>
    </div>
  </div>
}
