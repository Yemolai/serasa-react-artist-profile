/* eslint-disable-next-line no-unused-vars */
import React from 'react'

export const FeaturedArtist = props => {
  const { artist = {} } = props
  const styles = {
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center'
    },
    img: {
      border: '1px solid rgb(144, 144, 144, 0.4)',
      borderRadius: '50%',
      width: '190px',
      maxWidth: '46vw'
    },
    name: {
      color: 'rgb(14, 14, 14)',
      margin: '0.5em 0 0 0',
      textAlign: 'center'
    },
    genre: {
      color: 'rgba(144, 144, 144, 1)',
      margin: '0.2em 0',
      textAlign: 'center'
    }
  }
  return <div style={styles.wrapper}>
    <img style={styles.img} src={artist.imgUrl} alt="artist avatar"/>
    <p style={styles.name}>{artist.name}</p>
    <p style={styles.genre}>{artist.genre}</p>
  </div>
}
