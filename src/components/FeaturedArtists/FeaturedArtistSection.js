/* eslint-disable-next-line no-unused-vars */
import React from 'react'
import { useMediaQuery } from 'react-responsive'
import { FeaturedArtist } from './FeaturedArtist'

export const FeaturedArtistsSection = props => {
  const isMobile = useMediaQuery({
    query: `(max-device-width: 600px)`
  })
  const styles = {
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      borderTop: '1px solid rgba(144, 144, 144, 0.5)',
      margin: '0 10px',
      marginBottom: '20em'
    },
    title: {
      fontWeight: 700,
      fontFamily: `'Lato', sans-serif`
    },
    list: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    listItem: {
      margin: '0 0.5em'
    }
  }
  const featuredArtists = props.list
    .map((artist, k) => <FeaturedArtist styles={styles.listItem} key={k} artist={artist}/>)
  return <div style={styles.wrapper}>
    <h3 style={styles.title}>Featured Artists</h3>
    <div style={styles.list}>
      {isMobile ? featuredArtists.slice(0, 2) : featuredArtists}
    </div>
  </div>
}
