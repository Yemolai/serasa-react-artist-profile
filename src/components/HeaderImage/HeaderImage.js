/* eslint-disable-next-line no-unused-vars */
import React from 'react'

import placeholderImage from './placeholder-header.jpg'

export const HeaderImage = props => {
  const { src: url = placeholderImage } = props

  const halfAlphaDark = 'rgba(0, 0, 0, 0.1)'

  const halfAlphaDarkenGradient = `linear-gradient(${halfAlphaDark}, ${halfAlphaDark})`

  const styles = {
    image: {
      width: '100%',
      minHeight: '10em',
      maxHeight: '40em',
      height: '40vh',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      position: 'relative',
      backgroundImage: `${halfAlphaDarkenGradient}, url(${url})`
    }
  }

  return <div aria-label="page header" style={styles.image} />
}
