/* eslint-disable-next-line no-unused-vars */
import React from 'react'

const horizontalCenter = {
  justifyContent: 'center'
}

const verticalCenter = {
  alignItems: 'center'
}

export const CenterLayout = props => {
  const { horizontal, vertical, column, reverse } = props
  const styles = {
    wrapper: {
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: (column ? 'column' : 'row') + (reverse ? '-reverse' : ''),
      ...(!horizontal && !vertical ? { ...horizontalCenter, ...verticalCenter } : {}),
      ...(horizontal ? horizontalCenter : {}),
      ...(vertical ? verticalCenter : {}),
    }
  }
  return <div style={styles.wrapper}>
    {props.children}
  </div>
}
