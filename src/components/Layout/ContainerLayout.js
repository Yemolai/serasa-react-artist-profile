/* eslint-disable-next-line no-unused-vars */
import React from 'react'
import { useMediaQuery } from 'react-responsive'

export const ContainerLayout = props => {
  const isMobile = useMediaQuery({
    query: '(max-device-width: 600px)'
  })
  const styles = {
    container: {
      maxWidth: props.maxWidth ? props.maxWidth : '100%',
      width: props.width ? props.width : 'auto',
      marginLeft: 'auto',
      marginRight: 'auto',
      padding: isMobile ? 0 : '0 2em'
    }
  }
  return <div style={styles.container}>
    {props.children}
  </div>
}
