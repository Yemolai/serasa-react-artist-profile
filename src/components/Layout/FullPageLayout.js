/* eslint-disable-next-line no-unused-vars */
import React from 'react'

export const FullPageLayout = props => {
  const { height, width } = props
  const fullHeight = { height: '100vh' }
  const fullWidth = { width: '100vw' }
  const styles = {
    wrapper: {
      ...(!height && !width ? { ...fullHeight, ...fullWidth } : { height: 'auto' }),
      ...(height ? fullHeight : {}),
      ...(width ? fullWidth : { height: 'auto' }),
    }
  }
  return <div style={styles.wrapper}>
    {props.children}
  </div>
}
