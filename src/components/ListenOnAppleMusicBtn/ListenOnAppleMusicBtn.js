/* eslint-disable-next-line no-unused-vars */
import React from 'react'

const AppleRed = 'rgb(220, 60, 60)'

const styles = {
  btn: {
    color: AppleRed,
    background: 'transparent',
    border: '1px solid ' + AppleRed,
    borderRadius: '5px',
    padding: '10px',
    margin: '0.1em 0',
    fontFamily: `'Lato', sans-serif`,
    fontSize: '0.95em',
    letterSpacing: '0.5px',
    cursor: 'pointer'
  }
}


export const AppleMusicBtn = props => {
  const { verb = 'Listen' } = props
  return <button style={styles.btn} onClick={props.onClick}>
    {verb} on <b>Apple Music &#x2197;</b>
  </button>
}
