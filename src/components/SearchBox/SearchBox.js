/* eslint-disable-next-line no-unused-vars */
import React, { Component } from 'react'

export class SearchBox extends Component {
  styles = {
    wrapper: {
      display: 'flex',
      flexDirection: 'row'
    },
    btn: {
      background: 'transparent',
      border: '1px solid rgb(144, 144, 144, 0.5)',
      color: 'rgb(14, 14, 14)'
    },
    input: {
      borderRadius: '3px'
    }
  }

  state = {
    value: ''
  }

  handleInputChange = event => {
    const { value } = event.target
    this.setState({ value })
  }

  handleKeyUp = event => {
    const { onEnter = (() => undefined) } = this.props
    if (event.key === 'Enter') {
      onEnter(this.state.value)
    }
  }

  handleBtnClick = () => {
    const { onEnter = (() => undefined) } = this.props
    onEnter(this.state.value)
  }

  render () {
    return <div style={this.styles.wrapper}>
      <input
        style={this.styles.input}
        type="text"
        onChange={this.handleInputChange}
        onKeyUp={this.handleKeyUp}
        value={this.state.value}
      />
      <button
        style={this.styles.btn}
        onClick={this.handleBtnClick}
      >
        <span>Search</span>
      </button>
    </div>
  }
}
