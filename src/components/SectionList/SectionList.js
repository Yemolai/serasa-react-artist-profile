/* eslint-disable-next-line no-unused-vars */
import React from 'react'

const addLastProp = props => ({ ...props, last: true })
const addKeyProp = (props, key) => ({ ...props, key })

const setAsLast = (element, k) => {
  const props = addKeyProp(addLastProp(element.props), k)
  return React.cloneElement(element, props)
}

export const SectionList = props => {
  const styles = {
    ul: {
      padding: 0
    }
  }
  const lastChildIdx = props.children.length - 1
  return <ul style={styles.ul}>
    {props.children
      .map((item, idx) => idx < lastChildIdx
        ? addKeyProp(item, idx)
        : setAsLast(item, idx)
      )
    }
  </ul>
}
