/* eslint-disable-next-line no-unused-vars */
import React from 'react'

const lightGreyBorder = 'solid 1px rgba(144, 144, 144, 0.5)'

export const SectionListItem = props => {
  const styles = {
    li: {
      display: 'flex',
      flexDirection: 'column',
      listStylePosition: 'inside',
      borderTop: lightGreyBorder,
      padding: '0.2em 0'
    },
    label: {
      color: props.labelColor ? props.labelColor : 'rgb(144, 144, 144)',
      textTransform: 'uppercase',
      fontSize: '0.8em',
      fontWeight: 700,
      margin: '8px 0',
      marginBottom: '2px'
    },
    p: {
      margin: '8px 0',
      marginTop: '2px',
      fontSize: '1em'
    }
  }
  const label = props.label ? <p style={styles.label}>{props.label}</p> : ''
  return <li style={styles.li}>
    {label}
    <p style={styles.p}>{props.children}</p>
  </li>
}
