/* eslint-disable-next-line no-unused-vars */
import React, { Component } from 'react'
import moment from 'moment'
import { HeaderImage } from '../components/HeaderImage/HeaderImage'
import { ContainerLayout } from '../components/Layout/ContainerLayout'
import { FullPageLayout } from '../components/Layout/FullPageLayout'
import { ArtistBio } from '../components/ArtistBio/ArtistBio'
import { ArtistAlbumList } from './../components/ArtistAlbumList/ArtistAlbumList';
import { FeaturedArtistsSection } from '../components/FeaturedArtists/FeaturedArtistSection'

/**
 * Artist Profile Page with albums, songs and a little bio of the artist's work
 */
export class ArtistProfilePage extends Component {
  artist = {
    name: 'Lil Wayne',
    about: `A game-changing artist and celebrity, Lil Wayne began his career as a near-novelty -- a
    pre-teen delivering hardcore hip-hop -- but through years of maturation and reinventing the
    mixtape game, he developed into a million-selling rapper with a bassive body of work, one so
    inventive and cunning that it makes his famous claim of being the "best rapper alive" worh
    considering. Born Dwayne Michael Carter, Jr. and raised in the infamoud New Orleans neighborhood
    of Hollygrove, he was a straight-A student but never felt his true intelligence was expressed
    through any kind of report card.`.replace(/\s{2,}/gm, ' '),
    origin: 'New Orleans, LA',
    headerUri: '/lil-wayne-header.png',
    genre: 'Hip-Hop/Rap',
    birthDate: moment('1982-09-27').format('LL'),
    albums: [{
      title: 'Lil Wayne: Next Steps',
      coverUrl: '/lil-wayne-next-steps-cover.png',
      editorsNotes: `Apart from prison, there isn't much that has slowed down Lil Wayne's prolific
      creative output throught the '00s. Bizarre metaphors, tongue twister raps, and awesomely
      ridiculous couplets abound in these lesser-heard gems pulled from a...`,
      songs: [
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
      ]
    },{
      title: 'Lil Wayne: Next Steps',
      coverUrl: '/lil-wayne-next-steps-cover.png',
      editorsNotes: `Apart from prison, there isn't much that has slowed down Lil Wayne's prolific
      creative output throught the '00s. Bizarre metaphors, tongue twister raps, and awesomely
      ridiculous couplets abound in these lesser-heard gems pulled from a...`,
      songs: [
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is5-ssl.mzstatic.com/image/thumb/Music128/v4/e3/29/46/e329462d-0204-f77e-e610-11fe0fe9b6e9/00602527831626.rgb.jpg/146x0w.jpg',
          title: 'John (feat. Rick Ross)',
          duration: 286,
          artists: ['Lil Wayne']
        },
        {
          coverUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/31/c9/31/31c9311f-0df7-6de5-9eb1-173e4c774cb9/00602517834866.rgb.jpg/146x0w.jpg',
          title: 'Let the Beat Build',
          duration: 309,
          artists: ['Lil Wayne']
        },
      ]
    }]
  }
  featuredArtists = [
    { name: 'Lil Wayne', genre: 'Hip-Hop/Rap', imgUrl: 'https://is1-ssl.mzstatic.com/image/thumb/Features/v4/d8/b6/ad/d8b6add3-43f8-75ce-5761-2cf038bff0bf/mzl.ezxvmfrz.jpg/190x190cc.jpg' },
    { name: 'Birdman', genre: 'Hip-Hop/Rap', imgUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Features/v4/c5/32/a4/c532a457-ca9a-ced3-cdfe-8760488e90fb/mzl.vqtivtuo.jpg/135x135cc.jpg' },
    { name: 'B.G.', genre: 'Hip-Hop/Rap', imgUrl: 'https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/c2/ab/4e/c2ab4e40-8d1d-9672-47d0-43f9395e7a57/pr_source.png/190x190cc.jpg' },
    { name: 'Young Money', genre: 'Hip-Hop/Rap', imgUrl: 'https://is4-ssl.mzstatic.com/image/thumb/Features/v4/91/0a/5b/910a5ba8-85b1-7993-cce0-59c918c59c7d/mzl.xhhkaanu.png/190x190cc.jpg' },
    { name: 'Drake', genre: 'Hip-Hop/Rap', imgUrl: 'https://is2-ssl.mzstatic.com/image/thumb/Features115/v4/36/27/d1/3627d147-5033-c2f3-090f-5819e4f78107/mzl.rspsovgk.jpg/190x190cc.jpg' },
  ]
  render () {
    return (
      <FullPageLayout width>
        <HeaderImage src={this.artist.headerUri} />
        <ContainerLayout maxWidth="100%" width="90em">
          <ArtistBio artist={this.artist} />
        </ContainerLayout>
        <ContainerLayout maxWidth="100%" width="75em">
          <ArtistAlbumList artist={this.artist} />
          <FeaturedArtistsSection list={this.featuredArtists}/>
        </ContainerLayout>
      </FullPageLayout>
    )
  }
}
