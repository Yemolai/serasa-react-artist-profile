/* eslint-disable-next-line no-unused-vars */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { FullPageLayout } from '../components/Layout/FullPageLayout'
import { CenterLayout } from '../components/Layout/CenterLayout'
import { SearchBox } from '../components/SearchBox/SearchBox'
import { searchArtist } from '../services/AppleWebStoreService'
import { SectionList } from '../components/SectionList/SectionList'
import { SectionListItem } from '../components/SectionList/SectionListItem'

/**
 * Artist Search Box Page to look for an artist to present in the Artist Profile View
 */
export class ArtistSearchPage extends Component {
  styles = {
    link: {
      textDecoration: 'none',
      color: 'rgb(14, 14, 14)'
    }
  }
  state = {
    artistsList: []
  }
  navigateToDetails = id => {
    this.props.history.push(`/${id}`)
  }
  searchArtist = q => {
    searchArtist(q, { limit: 10 })
      .then(({ data }) => {
        this.setState({
          artistsList: data.results
        })
      })
      .catch(err => {
        alert(err.message)
        console.error('Error searching for artist', q, err)
      })
  }
  render () {
    const artistList = this.state.artistsList
      .map(({ artistId: id, artistName: name, primaryGenreName: genre }) =>{
        return <Link to={'/' + id} key={id} style={this.styles.link}>
          <SectionListItem label={genre}>
            {name}
          </SectionListItem>
        </Link>
      })
    return <FullPageLayout>
      <CenterLayout column>
        <p>Search for an artist to display</p>
        <SearchBox onEnter={this.searchArtist} />
        <SectionList>
          {artistList}
        </SectionList>
      </CenterLayout>
    </FullPageLayout>
  }
}
