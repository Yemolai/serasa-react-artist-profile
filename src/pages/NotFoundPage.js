/* eslint-disable-next-line no-unused-vars */
import React from 'react'
import { FullPageLayout } from '../components/Layout/FullPageLayout'
import { CenterLayout } from '../components/Layout/CenterLayout'

const SadFaceEmoji = () => <span role="img" aria-label="sad face emoji">😢</span>

export const NotFoundPage = () => (
  <FullPageLayout>
    <CenterLayout column>
      <h1>
        404
      </h1>
      <p>
        Page not Found <SadFaceEmoji/>
      </p>
    </CenterLayout>
  </FullPageLayout>
)
