import Axios from 'axios'

const itunesUrl = 'https://itunes.apple.com'
const httpItunes = Axios.create({
  baseURL: itunesUrl,
  timeout: 2000,
  headers: {
    ...Axios.defaults.headers.common,
    'Access-Control-Allow-Origin': 'http://tan-hands.surge.sh',
    'Content-Type': 'application/json'
  }
})

export const searchArtist = (artistName, params = {}) => {
  const queryParams = new URLSearchParams({
    term: artistName,
    media: 'music',
    country: 'br',
    entity: 'musicArtist',
    ...params
  })
  return httpItunes.get('/search?' + queryParams)
}

export const lookupArtist = ({ id, amgArtistId } = {}, params = {}) => {
  if (!id && !amgArtistId) {
    throw new Error('Must provide id or amdArtistId as lookup reference')
  }
  const queryParams = new URLSearchParams({
    ...(id ? { id } : amgArtistId ? { amgArtistId } : {}),
    ...params
  })
  return httpItunes.get('/lookup?' + queryParams)
}
