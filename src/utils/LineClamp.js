export const lineClamp = lines => ({
  display: '-webkit-box',
  'WebkitLineClamp': Number(lines),
  'WebkitBoxOrient': 'vertical',
  overflow: 'hidden'
})
